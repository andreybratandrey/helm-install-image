ARG ALPINE_VERSION

FROM alpine:${ALPINE_VERSION} AS base

FROM base AS build-base
RUN apk add --no-cache curl

FROM build-base AS kubectl
ARG KUBECTL_VERSION
ARG TARGETARCH
RUN mkdir /build
WORKDIR /build
ADD kubectl.sha512sum.${TARGETARCH} .
RUN curl -fLSs https://dl.k8s.io/v${KUBECTL_VERSION}/kubernetes-client-linux-${TARGETARCH}.tar.gz -o kubectl.tar.gz \
    && sha512sum -c kubectl.sha512sum.${TARGETARCH} \
    && tar -xvf kubectl.tar.gz

FROM build-base AS helm
ARG HELM3_VERSION
ARG TARGETARCH
RUN mkdir /build
WORKDIR /build
ADD helm3.sha256sum.${TARGETARCH} .
RUN curl -fLSs "https://get.helm.sh/helm-v$HELM3_VERSION-linux-${TARGETARCH}.tar.gz" -o helm3.tar.gz \
    && sha256sum -c helm3.sha256sum.${TARGETARCH} \
    && tar -xvf "helm3.tar.gz"

FROM build-base AS stage
ARG TARGETARCH
WORKDIR /stage
ENV PATH=$PATH:/stage/usr/bin
COPY --from=kubectl /build/kubernetes/client/bin/kubectl ./usr/bin/
COPY --from=helm /build/linux-${TARGETARCH}/helm ./usr/bin/

FROM base
# Standard fix so that golang's "netgo" checks /etc/hosts before doing DNS lookups
# See e.g. https://github.com/docker-library/docker/pull/84
RUN [ ! -e /etc/nsswitch.conf ] && echo 'hosts: files dns' > /etc/nsswitch.conf
RUN apk add --no-cache ca-certificates git
COPY --from=stage /stage/ /
